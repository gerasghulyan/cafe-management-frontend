(function () {
  'use strict';

  angular.module('app.__core')
    .constant('APP', {
      name: 'Application',
      mainState: 'home',
      authState: 'auth',
    });
})();
