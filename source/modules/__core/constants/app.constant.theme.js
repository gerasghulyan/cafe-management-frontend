(function () {
  'use strict';

  angular
    .module('app.__core')
    .constant('THEME', {
      sidebar: {
        palette: 'background',
        hue: 200
      },
      toolbar: {
        palette: 'primary',
        hue: 500
      }
    });
})();
