(function () {
  'use strict';

  angular
    .module('app.__core')
    .constant('API_CONFIG', {
      protocol: 'http://',
      hostname: 'localhost',
      port: '8089',
      pathname: '/api',
      getUrl: function () {
        var port = this.port ? ':' + this.port : this.port;
        return this.protocol + this.hostname + port + this.pathname;
      },
    });
})();
