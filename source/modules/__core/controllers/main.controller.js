(function () {
  'use strict';

  angular.module('app.__core').controller('MainController', controller);

  controller.$inject = ['$scope', '$location', '$log', 'APP', 'THEME', '$mdToast'];

  function controller($scope, $location, $log, APP, THEME, $mdToast) {
    $log.debug('Main controller started!');

    var main = this;
    main.appName = APP.name;
    main.THEME = THEME;

    $scope.$on('successfullyAuthorized', function () {
      $location.path('/');
    });

    $scope.$on('loggedOut', function () {
      $location.path('/');
    });

    $scope.$on('showAlert', function (e, data) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(data.template)
          .position('top right')
          .theme('dark-orange')
          .toastClass('error-toast')
          .hideDelay(2000)
      );
    });
  }
})();
