(function () {
  'use strict';

  angular.module('app.__core').config(config);

  config.$inject = ['$compileProvider'];

  function config($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
  }
})();
