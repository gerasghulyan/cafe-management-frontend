(function () {
  'use strict';

  angular
    .module('app.__core')
    .config(config);

  config.$inject = ['$mdThemingProvider'];

  function config($mdThemingProvider) {
    // red
    // pink
    // purple
    // deep-purple
    // indigo
    // blue
    // light-blue
    // cyan
    // teal
    // green
    // light-green
    // lime
    // yellow
    // amber
    // orange
    // deep-orange
    // brown
    // grey
    // blue-grey
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('amber')
      .warnPalette('red')
      .backgroundPalette('grey');

    $mdThemingProvider.theme('alternative')
      .primaryPalette('blue-grey')
      .accentPalette('grey')
      .warnPalette('deep-orange')
      .backgroundPalette('grey');

    $mdThemingProvider.theme('dark-orange')
      .backgroundPalette('orange')
      .dark();

    $mdThemingProvider.theme('dark-red')
      .backgroundPalette('red')
      .dark();

    $mdThemingProvider.theme('dark-teal')
      .backgroundPalette('teal')
      .dark();

    $mdThemingProvider.theme('dark-yellow')
      .backgroundPalette('yellow')
      .dark();

    $mdThemingProvider.theme('dark-green')
      .backgroundPalette('green')
      .dark();

    $mdThemingProvider.theme('dark-blue-grey')
      .backgroundPalette('blue-grey')
      .dark();

    $mdThemingProvider.theme('green')
      .primaryPalette('green')
      .accentPalette('amber')
      .warnPalette('red')
      .backgroundPalette('grey');

    $mdThemingProvider.theme('monochrome')
      .primaryPalette('grey')
      .accentPalette('grey')
      .warnPalette('grey')
      .backgroundPalette('grey');

    $mdThemingProvider.alwaysWatchTheme(true);

    $mdThemingProvider.setDefaultTheme('default');
  }
})();
