(function () {
  'use strict';

  angular.module('app.__core').config(config);

  config.$inject = [
    '$sceProvider',
    '$urlMatcherFactoryProvider',
    '$urlRouterProvider',
    '$locationProvider'];

  function config($sceProvider,
                  $urlMatcherFactoryProvider,
                  $urlRouterProvider,
                  $locationProvider) {
    $sceProvider.enabled(false);
    $urlMatcherFactoryProvider.strictMode(false);

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: false,
      requireBase: true,
    });

    $locationProvider.hashPrefix('!');
  }
})();
