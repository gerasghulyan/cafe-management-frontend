(function () {
  'use strict';

  angular.module('app.__core').config(config);

  config.$inject = ['$logProvider'];

  function config($logProvider) {
    $logProvider.debugEnabled(true);
  }
})();
