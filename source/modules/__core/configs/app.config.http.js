(function () {
  'use strict';

  angular.module('app.__core').config(config);

  config.$inject = ['$httpProvider'];

  function config($httpProvider) {
    $httpProvider.useApplyAsync(true);
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
  }
})();
