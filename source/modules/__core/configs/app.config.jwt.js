(function () {
  'use strict';

  angular
    .module('app.__core')
    .config(config);

  config.$inject = ['jwtInterceptorProvider',
    '$httpProvider',
    'API_CONFIG',
    'APP'];

  function config(jwtInterceptorProvider,
                  $httpProvider,
                  API_CONFIG,
                  APP) {

    tokenGetter.$inject = ['options',
      'jwtHelper',
      'AuthFactory',
      '$window',
      '$log'];

    function tokenGetter(options, jwtHelper, AuthFactory, $window, $log) {
      if (options.url.substr(options.url.length - 5) === '.html') {
        return;
      }
      var token = $window.localStorage.getItem('access_token');
      if (token) {
        try {
          if (!jwtHelper.isTokenExpired(token)) {
            return token;
          }
        } catch (err) {
          $log.error(err);
          return token;
        }
      }
    }

    unauthenticatedRedirector.$inject = ['$state'];

    function unauthenticatedRedirector($state) {
      $state.go(APP.authState);
    }

    jwtInterceptorProvider.tokenGetter = tokenGetter;
    jwtInterceptorProvider.authHeader = 'X-AUTH-TOKEN';
    jwtInterceptorProvider.authPrefix = '';
    jwtInterceptorProvider.whiteListedDomains = [
      API_CONFIG.hostname,
      'localhost',
    ];
    jwtInterceptorProvider
      .unauthenticatedRedirector = unauthenticatedRedirector;

    $httpProvider.interceptors.push('jwtInterceptor');
  }
})();
