(function () {
  'use strict';

  angular
    .module('app.__core')
    .factory('AuthFactory', factory);

  factory.$inject = ['$http',
    '$window',
    '$location',
    'jwtHelper',
    'API_CONFIG',
    'APP'];

  function factory($http,
                   $window,
                   $location,
                   jwtHelper,
                   API_CONFIG,
                   APP) {

    var factory = {
      auth: auth,
      isAuthenticated: isAuthenticated,
      isExpired: isExpired,
      logout: logout,
      getCurrentUser: getCurrentUser
    };

    return factory;

    function auth(username, password) {
      return $http({
        method: 'POST',
        url: API_CONFIG.getUrl() + '/auth/login',
        skipAuthorization: true,
        data: {
          username: username,
          password: password,
        },
      })
        .then(function (data) {
          var token = data.headers()['x-auth-token'];
          $window.localStorage.setItem('access_token', token);
        });
    }

    function isAuthenticated() {
      return !!$window.localStorage.getItem('access_token');
    }

    function isExpired() {
      var token = $window.localStorage.getItem('access_token');
      return !!(token && jwtHelper.isTokenExpired(token));
    }

    function logout() {
      $window.localStorage.removeItem('access_token');
      $location.path(APP.authState);
    }

    function getCurrentUser() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/users/auth',
      });
    }
  }
})();
