(function () {
  'use strict';

  angular.module('app.__core').factory('CheckModulesFactory', factory);

  factory.$inject = ['$state'];

  function factory($state) {
    var factory = {
      exist: exist,
      isState: isState,
    };

    return factory;

    function exist(modules) {
      if (typeof modules === 'string') {
        modules = [modules];
      }
      var module;
      for (var i = 0; i < modules.length; i++) {
        try {
          module = angular.module(modules[0]);
        } catch (err) {
          return false;
        }
      }
      return true;
    }

    function isState(state) {
      var states = $state.get();
      for (var i = states.length; i--;) {
        if (state === states[i].name) {
          return true;
        }
      }
      return false;
    }
  }
})();
