(function () {
  'use strict';

  angular
    .module('app.__core')
    .factory('MenuFactory', factory);

  factory.$inject = ['$state'];

  function factory($state) {

    var factory = {
      selectSection: selectSection,
      toggleSelectSection: toggleSelectSection,
      isSectionSelected: isSectionSelected,
      selectPage: selectPage,
      isPageSelected: isPageSelected
    };

    var openedSection;
    var currentSection;
    var currentPage;

    return factory;

    function selectSection(section) {
      openedSection = section;
    }

    function toggleSelectSection(section) {
      openedSection = (openedSection === section ? null : section);
    }

    function isSectionSelected(section) {
      if (openedSection) {
        return openedSection === section;
      } else {
        for (var i = section.pages.length; i--;) {
          if ($state.current.name === section.pages[i].state) {
            return true;
          }
        }
        return false;
      }
    }

    function selectPage(section, page) {
      currentSection = section;
      currentPage = page;
    }

    function isPageSelected(page) {
      return $state.current.name === page.state;
    }
  }
})();
