(function () {
  'use strict';

  angular.module('app.__core').service('SharedPropertiesService', service);

  service.$inject = [];

  function service() {
    var properties = {};

    var service = {
      getProperty: getProperty,
      setProperty: setProperty,
    };
    return service;

    function getProperty(name) {
      if (angular.isDefined(properties[name])) {
        return properties[name];
      } else {
        return undefined;
      }
    }

    function setProperty(name, value) {
      properties[name] = value;
    }
  }
})();
