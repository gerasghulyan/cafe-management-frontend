(function () {
  'use strict';

  angular.module('app.__core').filter('nospace', filter);

  filter.$inject = [];

  function filter() {
    return function (value) {
      return (!value) ? '' : value.replace(/ /g, '');
    };
  }
})();
