(function () {
  'use strict';

  angular
    .module('app.__core')
    .filter('humanizeDoc', filter);

  filter.$inject = [];

  function filter() {
    return function (doc) {
      if (!doc) return;
      if (doc.type === 'directive') {
        return doc.name.replace(/([A-Z])/g, function ($1) {
          return '-' + $1.toLowerCase();
        });
      }
      return doc.label || doc.name;
    };
  }
})();
