(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('menuLink', directive);

  directive.$inject = ['CheckModulesFactory', 'MenuFactory'];

  function directive(CheckModulesFactory, MenuFactory) {
    return {
      scope: {
        section: '='
      },
      templateUrl: '__core/directives/menu-link.tmpl.html',
      link: function ($scope, $element) {
        var controller = $element.parent().controller();

        $scope.isSelected = function () {
          return MenuFactory.isPageSelected($scope.section);
        };

        $scope.focusSection = function () {
          // set flag to be used later when
          // $locationChangeSuccess calls openPage()
          controller.autoFocusContent = true;
        };

        $scope.isState = function (state) {
          return CheckModulesFactory.isState(state);
        };
      }
    };
  }
})();
