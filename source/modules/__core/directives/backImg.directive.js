(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('backImg', directive);

  directive.$inject = [];

  function directive() {
    return {
      link: function ($scope, $element, $attrs) {
        $attrs.$observe('backImg', function (value) {
          $element.css({
            'background-image': 'url(' + encodeURI(value) + ')'
          });
        });
      }
    };
  }
})();
