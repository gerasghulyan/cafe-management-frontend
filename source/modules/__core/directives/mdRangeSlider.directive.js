(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('mdRangeSlider', directive);

  directive.$inject = [];

  function directive() {
    return {
      restrict: 'E',
      scope: {
        max: '=',
        min: '=',
        minGap: '=',
        step: '=',
        lowerValue: '=lowerValue',
        upperValue: '=upperValue',
        class: '@'
      },
      template: [
        '<div class="range-slider-container">',
        '<div class="range-slider-left">',
        '<md-slider md-discrete aria-label="upperValue" step="{{step}}" ' +
        'ng-model="lowerValue" min="{{min}}" max="{{lowerMax}}" ' +
        'class="{{class}}"></md-slider>',
        '</div>',
        '<div class="range-slider-right" ng-style="{width: upperWidth}">',
        '<md-slider md-discrete aria-label="upperValue" step="{{step}}" ' +
        'ng-model="upperValue" min="{{upperMin}}" max="{{max}}"' +
        'class="{{class}}"></md-slider>',
        '</div>',
        '</div>'
      ].join(''),
      link: function ($scope, $element) {
        if (!$scope.step) {
          $scope.step = 1;
        }

        if (angular.isUndefined($scope.minGap)) {
          $scope.minGap = $scope.step;
        }

        $scope.$watchGroup(['min', 'max'], minMaxWatcher);
        $scope.$watch('lowerValue', lowerValueWatcher);
        $scope.$watch('upperValue', upperValueWatcher);

        function minMaxWatcher() {
          $scope.lowerMax = $scope.max - $scope.minGap;
          $scope.upperMin = $scope.lowerValue + $scope.minGap;

          if (!$scope.lowerValue || $scope.lowerValue < $scope.min) {
            $scope.lowerValue = $scope.min;
          } else {
            $scope.lowerValue *= 1;
          }
          if (!$scope.upperValue || $scope.upperValue > $scope.max) {
            $scope.upperValue = $scope.max;
          } else {
            $scope.upperValue *= 1;
          }
          updateWidth();
        }

        function lowerValueWatcher() {
          if ($scope.lowerValue >= $scope.upperValue - $scope.minGap) {
            $scope.lowerValue = $scope.upperValue - $scope.minGap;
            return;
          }
          $scope.upperMin = $scope.lowerValue + $scope.minGap;

          updateWidth();
        }

        function upperValueWatcher() {
          if ($scope.upperValue <= $scope.lowerValue + $scope.minGap) {
            $scope.upperValue = $scope.lowerValue + $scope.minGap;
          }
        }

        function updateWidth() {
          $scope.upperWidth =
            ((($scope.max - ($scope.lowerValue + $scope.minGap)) /
              ($scope.max - $scope.min)) * 100) + '%';
          if ($scope.lowerValue > ($scope.upperValue - $scope.minGap) &&
            $scope.upperValue < $scope.max) {
            $scope.upperValue = $scope.lowerValue + $scope.minGap;
          }
        }
      }
    };
  }
})();
