(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('menuToggle', directive);

  directive.$inject = ['$timeout',
    '$mdUtil',
    'CheckModulesFactory',
    'MenuFactory'];

  function directive($timeout,
                     $mdUtil,
                     CheckModulesFactory,
                     MenuFactory) {
    return {
      scope: {
        section: '='
      },
      templateUrl: '__core/directives/menu-toggle.tmpl.html',
      link: function ($scope, $element) {
        $scope.isOpen = function () {
          return MenuFactory.isSectionSelected($scope.section);
        };

        $scope.toggle = function () {
          MenuFactory.toggleSelectSection($scope.section);
        };

        $scope.isState = function (state) {
          return CheckModulesFactory.isState(state);
        };

        $mdUtil.nextTick(function () {
          $scope.$watch(
            function () {
              return MenuFactory.isSectionSelected($scope.section);
            },
            function (open) {
              $mdUtil.nextTick(function () {
                var $ul = $element.find('ul');
                var $li = $ul[0].querySelector('a.active');
                var docsMenuContent =
                  document.querySelector('.docs-menu').parentNode;
                var targetHeight = open ? getTargetHeight() : 0;

                $timeout(function () {
                  $ul.css({height: targetHeight + 'px'});
                  if (open && $li && $li.offsetParent &&
                    $ul[0].scrollTop === 0) {
                    $timeout(function () {
                      var activeHeight = $li.scrollHeight;
                      var activeOffset = $li.offsetTop;
                      var parentOffset = $li.offsetParent.offsetTop;
                      var negativeOffset = activeHeight * 2;
                      var newScrollTop = activeOffset + parentOffset
                        - negativeOffset;
                      $mdUtil.animateScrollTo(docsMenuContent, newScrollTop);
                    }, 350, false);
                  }
                }, 0, false);

                function getTargetHeight() {
                  var targetHeight;
                  $ul.addClass('no-transition');
                  $ul.css('height', '');
                  targetHeight = $ul.prop('clientHeight');
                  $ul.css('height', 0);
                  $ul.removeClass('no-transition');
                  return targetHeight;
                }
              }, false);
            }
          );
        });

        var parentNode = $element[0].parentNode.parentNode.parentNode;
        if (parentNode.classList.contains('parent-list-item')) {
          var heading = parentNode.querySelector('h2');
          $element[0].firstChild.setAttribute('aria-describedby', heading.id);
        }
      }
    };
  }
})();
