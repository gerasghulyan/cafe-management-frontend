(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('selectOnClick', directive);

  directive.$inject = ['$window'];

  function directive($window) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.on('click', function () {
          if (!$window.getSelection().toString()) {
            this.setSelectionRange(0, this.value.length);
          }
        });
      }
    };
  }
})();
