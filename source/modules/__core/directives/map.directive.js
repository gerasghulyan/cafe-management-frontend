(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('map', directive);

  directive.$inject = [];

  function directive() {
    return {
      restrict: 'E',
      scope: {
        onCreate: '&',
        lat: '=',
        lng: '=',
        title: '=',
        address: '='
      },
      link: function ($scope, $element) {
        function initialize() {
          var lat = $scope.lat || 0;
          var lng = $scope.lng || 0;
          var title = $scope.title || '';
          var address = $scope.address || '';

          var myLatLng = new google.maps.LatLng(lat, lng);

          var mapOptions = {
            center: myLatLng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          var map = new google.maps.Map($element[0], mapOptions);
          map.marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: title
          });
          map.infoWindow = new google.maps.InfoWindow({
            content: '<h4 class="text-center">' + title + '</h4>' +
            '<p>' + address + '</p>'
          });

          $scope.onCreate({map: map});

          google.maps.event.addListener(map.marker, 'click', function () {
            map.infoWindow.open(map, map.marker);
          });

          google.maps.event.addListener(map.infoWindow, 'click', function () {
            $scope.onInfoWindowClick({map: map});
          });

          google.maps.event.addDomListener($element[0], 'mousedown',
            function (e) {
              e.preventDefault();
              return false;
            });
        }

        if (document.readyState === 'complete') {
          initialize();
        } else {
          google.maps.event.addDomListener(window, 'load', initialize);
        }
      }
    };
  }
})();
