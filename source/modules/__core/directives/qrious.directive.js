(function () {
  'use strict';

  angular
    .module('app.__core')
    .directive('qrious', directive);

  directive.$inject = [];

  function directive() {
    return {
      restrict: 'E',
      template: '<img>',
      link: function ($scope, $element, $attr) {
        function draw() {
          return new QRious({
            element: $element.children()[0],
            value: $attr.value || '',
            level: $attr.level || 'H',
            size: parseInt($attr.size) || 256,
            background: $attr.background || '#fff',
            foreground: $attr.foreground || '#000'
          });
        }

        $attr.$observe('value', function () {
          draw();
        });
        $attr.$observe('level', function () {
          draw();
        });
        $attr.$observe('size', function () {
          draw();
        });
        $attr.$observe('background', function () {
          draw();
        });
        $attr.$observe('foreground', function () {
          draw();
        });
      }
    };
  }
})();
