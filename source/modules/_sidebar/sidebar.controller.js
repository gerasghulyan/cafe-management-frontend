(function () {
  'use strict';

  angular.module('app.__core').controller('SidebarController', controller);

  controller.$inject = ['$rootScope',
    '$mdSidenav',
    'CheckModulesFactory',
    'AuthFactory',
    '$log'];

  function controller($rootScope,
                      $mdSidenav,
                      CheckModulesFactory,
                      AuthFactory,
                      $log) {
    var vm = this;
    vm.moduleExists = moduleExists;
    vm.isState = isState;
    vm.accessList = [];
    vm.isAccessible = isAccessible;

    var sidebarCloseRequestListener = $rootScope.$on('sidebarCloseRequest',
      function () {
        closeSidebar();
      });

    function closeSidebar() {
      $mdSidenav('sidebar').close()
        .then(function () {
        });
    }

    function moduleExists(modules) {
      return CheckModulesFactory.exist(modules);
    }

    function isState(state) {
      return CheckModulesFactory.isState(state);
    }

    activate();

    function activate() {
      getUserAccessList();
    }

    function getUserAccessList() {
      AuthFactory.getCurrentUser()
        .then(function (data) {
          vm.accessList = data.data.role;
        })
        .catch($log.log.bind($log));
    }

    function isAccessible(section) {
      if (section.access && section.access !== '') {
        return (vm.accessList === section.access);
      } else {
        return true;
      }
    }

    vm.menu = [
      {
        name: 'Users',
        state: 'users',
        type: 'toggle',
        icon: 'fa-users',
        access: 'MANAGER',
        pages: [
          {
            name: 'All Users',
            state: 'users.all',
            type: 'link',
            icon: 'fa-server',
          },
          {
            name: 'Add User',
            state: 'users.add',
            type: 'link',
            icon: 'fa-user-plus',
          },
        ],
      },
      {
        name: 'Tables',
        state: 'tables',
        type: 'toggle',
        icon: 'fa-table',
        access: 'MANAGER',
        pages: [
          {
            name: 'All Tables',
            state: 'tables.all',
            type: 'link',
            icon: 'fa-table',
          },
          {
            name: 'Add Table',
            state: 'tables.add',
            type: 'link',
            icon: 'fa-plus-square-o',
          },
        ],
      },
      {
        name: 'Products',
        state: 'products',
        type: 'toggle',
        icon: 'fa-product-hunt',
        access: 'MANAGER',
        pages: [
          {
            name: 'All Products',
            state: 'products.all',
            type: 'link',
            icon: 'fa-product-hunt',
          },
          {
            name: 'Add Product',
            state: 'products.add',
            type: 'link',
            icon: 'fa-plus-square-o',
          },
        ],
      },
      {
        name: 'Tables',
        state: 'tables',
        type: 'toggle',
        icon: 'fa-table',
        access: 'WAITER',
        pages: [
          {
            name: 'Assigned Tables',
            state: 'tables.assigned',
            type: 'link',
            icon: 'fa-table',
          }
        ],
      },
      {
        name: 'Orders',
        state: 'orders',
        type: 'toggle',
        icon: 'fa-bars',
        access: 'WAITER',
        pages: [
          {
            name: 'All Orders',
            state: 'orders.all',
            type: 'link',
            icon: 'fa-bars',
          },
          {
            name: 'Add Order',
            state: 'orders.add',
            type: 'link',
            icon: 'fa-plus-square-o',
          },
        ],
      }
    ];
  }
})();
