(function () {
  'use strict';

  angular.module('app', [
    'ngMaterial',
    'ui.router',
    'templates',
    'angular-jwt',
    'ngIdle',
    'angular-loading-bar',
    'hmTouchEvents',
    'md.data.table',

    'app.__core',
    'app.home',
    'app.auth',

    'app.users',
    'app.tables',
    'app.products',
    'app.orders'
  ]);
})();
