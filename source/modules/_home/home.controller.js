(function () {
  'use strict';

  angular.module('app.home').controller('HomeController', controller);

  controller.$inject = ['$log'];

  function controller($log) {
    $log.debug('Home loaded!');
    var vm = this;

    activate();

    function activate() {

    }
  }
})();
