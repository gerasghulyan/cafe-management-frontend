(function () {
  'use strict';

  angular.module('app.home').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state(
        {
          name: 'home',
          url: '/',
          templateUrl: '_home/home.html',
          controllerAs: 'vm',
          controller: 'HomeController',
          data: {
            publicAccess: false,
            authorizedAccess: true,
          },
        });
  }
})();
