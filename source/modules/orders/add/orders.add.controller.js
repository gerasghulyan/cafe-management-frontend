(function () {
  'use strict';

  angular
    .module('app.orders')
    .controller('OrdersAddController', controller);

  controller.$inject = [
    '$scope',
    '$state',
    '$log',
    '$timeout',
    '$window',
    'OrdersFactory',
    'ProductsFactory',
    'TablesFactory'];

  function controller($scope,
                      $state,
                      $log,
                      $timeout,
                      $window,
                      OrdersFactory,
                      ProductsFactory,
                      TablesFactory) {
    var vm = this;
    vm.addOrder = addOrder;

    activate();

    function activate() {
      ProductsFactory.getProducts()
        .then(function (data) {
          vm.products = data.data;
        })
        .catch($log.log.bind($log));

      TablesFactory.getAssignedTables()
        .then(function (data) {
          vm.tables = data.data;
        })
        .catch($log.log.bind($log));
    }

    function addOrder() {
      $log.log(vm.tab);
      OrdersFactory.createOrder({
        table: vm.table,
        products: vm.products,
      })
        .then(function (data) {
          $state.go('orders.all');
        })
        .catch(function (err) {
          $scope.$emit('showAlert', {
            title: 'Error',
            template: (err.data && err.data.message) ?
              err.data.message :
              angular.toJson(err.data),
          });
        });
    }
  }
})();
