(function () {
  'use strict';

  angular
    .module('app.orders')
    .factory('OrdersFactory', factory);

  factory.$inject = ['$http', 'API_CONFIG'];

  function factory($http, API_CONFIG) {
    var factory = {
      getOrders: getOrders,
      createOrder: createOrder,
      updateOrder: updateOrder
    };

    return factory;

    function getOrders() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/orders',
      });
    }

    function createOrder(data) {
      return $http({
        method: 'POST',
        url: API_CONFIG.getUrl() + '/tables/' + data.table,
        data: data.products,
      });
    }

    function updateOrder(order) {
      return $http({
        method: 'PUT',
        url: API_CONFIG.getUrl() + '/order',
        data: order
      });
    }

  }
})();
