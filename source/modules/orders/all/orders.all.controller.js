(function () {
  'use strict';

  angular
    .module('app.orders')
    .controller('OrdersAllController', controller);

  controller.$inject = [
    '$log',
    'OrdersFactory'];

  function controller($log,
                      OrdersFactory) {
    var vm = this;

    activate();

    function activate() {
      getOrders();
    }

    function getOrders() {
      OrdersFactory.getOrders()
        .then(function (data) {
          vm.orders = data.data;
        })
        .catch($log.log.bind($log));
    }
  }
})();
