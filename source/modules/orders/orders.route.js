(function () {
  'use strict';

  angular.module('app.orders').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('orders', {
        url: '/orders',
        abstract: true,
        templateUrl: 'orders/orders.layout.html',
        redirectTo: 'orders.all',
      })
      .state('orders.add', {
        url: '/add',
        controller: 'OrdersAddController',
        controllerAs: 'vm',
        templateUrl: 'orders/add/orders.add.html',
        data: {
          title: 'Add Order',
          publicAccess: false,
        },
      })
      .state('orders.all', {
        url: '/all',
        controller: 'OrdersAllController',
        controllerAs: 'vm',
        templateUrl: 'orders/all/orders.all.html',
        data: {
          title: 'All Orders',
          publicAccess: false,
        },
      })
    ;
  }
})();
