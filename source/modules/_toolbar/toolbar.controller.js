(function () {
  'use strict';

  angular.module('app.__core').controller('ToolbarController', controller);

  controller.$inject = [
    '$mdSidenav',
    '$location',
    'AuthFactory',
    'CheckModulesFactory',
    // 'ProfileFactory',
    '$log',
    '$rootScope',
    '$mdDialog',
  ];

  function controller($mdSidenav,
                      $location,
                      AuthFactory,
                      CheckModulesFactory,
                      // ProfileFactory,
                      $log,
                      $rootScope,
                      $mdDialog) {
    var vm = this;
    vm.getRouteClass = getRouteClass;
    vm.toggleSidebar = toggleSidebar;
    vm.logout = logout;
    vm.isState = isState;
    vm.openMenu = openMenu;
    vm.showWelcomeTourDialog = showWelcomeTourDialog;

    activate();

    function activate() {
      vm.language = 'EN';
      if (AuthFactory.isAuthenticated()) {
        AuthFactory.getCurrentUser()
          .then(function (data) {
            vm.profile = data.data;
          })
          .catch($log.log.bind($log));
      }
      // ProfileFactory.getProfile()
      //   .then(function (data) {
      //     vm.profile = data.data;
      //   })
      //   .catch($log.log.bind($log))
      //   .finally(function () {
      //     vm.isLoading = false;
      //   });
    }

    $rootScope.$on('profileUpdated', function () {
      activate();
    });

    function toggleSidebar() {
      buildToggle('sidebar');
    }

    function buildToggle(navID) {
      $mdSidenav(navID)
        .toggle()
        .then(function () {
        });
    }

    function getRouteClass(path, classString) {
      var classStr = classString || 'active';
      return ($location.path().substr(0, path.length) === path) ? classStr : '';
    }

    function logout() {
      AuthFactory.logout();
    }

    function isState(state) {
      return CheckModulesFactory.isState(state);
    }

    var originatorEv;

    function openMenu($mdMenu, ev) {
      originatorEv = ev;
      $mdMenu.open(ev);
    }

    $rootScope.$on('messageRead', function () {
      activate();
    });

    function showWelcomeTourDialog() {
      $mdDialog.show({
        controller: 'WelcomeTourDialogController',
        controllerAs: 'vm',
        templateUrl: '_footer/welcome-tour/welcome-tour.dialog.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        fullscreen: true
      });
    }
  }
})();
