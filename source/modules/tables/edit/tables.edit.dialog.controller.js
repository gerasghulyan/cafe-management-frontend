(function () {
  'use strict';

  angular
    .module('app.tables')
    .controller('TableEditDialogController', controller);

  controller.$inject = [
    '$log',
    '$mdDialog',
    '$table',
    '$mdToast',
    'UsersFactory',
    'TablesFactory'
  ];

  function controller($log,
                      $mdDialog,
                      $table,
                      $mdToast,
                      UsersFactory,
                      TablesFactory) {
    var vm = this;

    vm.table = angular.copy($table);
    vm.save = save;

    activate();

    function activate() {
      UsersFactory.getWaiterUsers()
        .then(function (data) {
          vm.waiters = data.data;
          vm.waiter = vm.table.user;
        })
        .catch($log.log.bind($log));
    }

    function save() {
      TablesFactory.updateTable(
        vm.table
      )
        .then(function (data) {
          $mdDialog.hide();
          $mdToast.show(
            $mdToast.simple()
              .textContent('Table updated')
              .position('top right')
              .hideDelay(3000)
          );
        })
        .catch(function (err) {
          var errors = err.data.errors;
          $mdToast.show(
            $mdToast.simple()
              .textContent(errors[0].message)
              .position('top right')
              .theme('dark-orange')
              .toastClass('error-toast')
              .hideDelay(2000)
          );
        });
    }
  }
})();
