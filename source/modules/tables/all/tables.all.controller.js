(function () {
  'use strict';

  angular
    .module('app.tables')
    .controller('TablesAllController', controller);

  controller.$inject = [
    '$log',
    '$mdDialog',
    'TablesFactory'];

  function controller($log,
                      $mdDialog,
                      TablesFactory) {
    var vm = this;
    vm.editTable = editTable;

    activate();

    function activate() {
      getTables();
    }

    function getTables() {
      TablesFactory.getTables()
        .then(function (data) {
          vm.tables = data.data;
        })
        .catch($log.log.bind($log));
    }

    function editTable(table, $event) {
      $mdDialog.show({
        controller: 'TableEditDialogController',
        controllerAs: 'vm',
        templateUrl: 'tables/edit/tables.edit.dialog.html',
        parent: angular.element(document.body),
        targetEvent: $event,
        clickOutsideToClose: true,
        fullscreen: true,
        locals: {
          '$table': table
        }
      })
        .then(function (answer) {
          activate();
          $log.debug('Table saved');
        }, function () {
          $log.debug('Dialog cancelled');
        });
    }
  }
})();
