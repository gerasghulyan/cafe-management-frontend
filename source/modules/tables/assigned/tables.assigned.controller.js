(function () {
  'use strict';

  angular
    .module('app.tables')
    .controller('TablesAssignedController', controller);

  controller.$inject = [
    '$log',
    'TablesFactory'];

  function controller($log,
                      TablesFactory) {
    var vm = this;

    activate();

    function activate() {
      getAssignedTables();
    }

    function getAssignedTables() {
      TablesFactory.getAssignedTables()
        .then(function (data) {
          vm.tables = data.data;
        })
        .catch($log.log.bind($log));
    }
  }
})();
