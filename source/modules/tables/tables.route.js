(function () {
  'use strict';

  angular.module('app.tables').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('tables', {
        url: '/tables',
        abstract: true,
        templateUrl: 'tables/tables.layout.html',
        redirectTo: 'tables.all',
      })
      .state('tables.add', {
        url: '/add',
        controller: 'TablesAddController',
        controllerAs: 'vm',
        templateUrl: 'tables/add/tables.add.html',
        data: {
          title: 'Add Table',
          publicAccess: false,
        },
      })
      .state('tables.all', {
        url: '/all',
        controller: 'TablesAllController',
        controllerAs: 'vm',
        templateUrl: 'tables/all/tables.all.html',
        data: {
          title: 'All Tables',
          publicAccess: false,
        },
      })
      .state('tables.assigned', {
        url: '/assigned',
        controller: 'TablesAssignedController',
        controllerAs: 'vm',
        templateUrl: 'tables/assigned/tables.assigned.html',
        data: {
          title: 'All Tables',
          publicAccess: false,
        },
      })
    ;
  }
})();
