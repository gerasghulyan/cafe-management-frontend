(function () {
  'use strict';

  angular
    .module('app.tables')
    .factory('TablesFactory', factory);

  factory.$inject = ['$http', 'API_CONFIG'];

  function factory($http, API_CONFIG) {
    var factory = {
      getTables: getTables,
      assignTable: assignTable,
      getAssignedTables: getAssignedTables,
      createTable: createTable,
      updateTable: updateTable,

    };

    return factory;

    function getTables() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/tables',
      });
    }

    function assignTable(table, user) {
      return $http({
        method: 'PUT',
        url: API_CONFIG.getUrl() + '/tables',
        data: {table: table}
      });
    }

    function getAssignedTables() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/tables/assigned',
      });
    }

    function createTable(table) {
      return $http({
        method: 'POST',
        url: API_CONFIG.getUrl() + '/tables',
        data: table,
      });
    }

    function updateTable(table) {
      return $http({
        method: 'PUT',
        url: API_CONFIG.getUrl() + '/tables',
        data: table
      });
    }
  }
})();
