(function () {
  'use strict';

  angular
    .module('app.tables')
    .controller('TablesAddController', controller);

  controller.$inject = [
    '$scope',
    '$state',
    'TablesFactory'];

  function controller($scope,
                      $state,
                      TablesFactory) {
    var vm = this;
    vm.addTable = addTable;

    function addTable() {
      TablesFactory.createTable({
        name: vm.name
      })
        .then(function (data) {
          $state.go('tables.all');
        })
        .catch(function (err) {
          $scope.$emit('showAlert', {
            title: 'Error',
            template: (err.data && err.data.message) ?
              err.data.message :
              angular.toJson(err.data),
          });
        });
    }
  }
})();
