(function () {
  'use strict';

  angular.module('app.auth').controller('AuthController', controller);

  controller.$inject = ['$log', 'AuthFactory', '$scope'];

  function controller($log, AuthFactory, $scope) {
    $log.debug('Auth loaded!');
    var vm = this;
    vm.authenticate = authenticate;

    activate();

    function activate() {
      vm.errors = {};
    }

    function authenticate() {
      if (!vm.username) {
        vm.errors.username = 'You must specify the username';
        return;
      } else {
        delete vm.errors.username;
      }

      if (!vm.password) {
        vm.errors.password = 'You must specify the password';
        return;
      } else {
        delete vm.errors.password;
      }

      AuthFactory.auth(vm.username, vm.password)
        .then(function () {
          $scope.$emit('successfullyAuthorized');
        })
        .catch(function (err) {
          $scope.$emit('showAlert', {
            title: 'Error',
            template: (err.data && err.data.message) ?
              err.data.message :
              angular.toJson(err.data),
          });
        });

      var usernameWatcher = $scope.$watch('vm.username', function (newVal) {
        if (newVal) {
          delete vm.errors.username;
        }
      });

      var passwordWatcher = $scope.$watch('vm.password', function (newVal) {
        if (newVal) {
          delete vm.errors.password;
        }
      });

      var scopeDestroyWatcher = $scope.$on('$destroy', function () {
        usernameWatcher();
        passwordWatcher();
        scopeDestroyWatcher();
      });
    }
  }
})();
