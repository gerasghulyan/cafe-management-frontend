(function () {
  'use strict';

  angular.module('app.auth').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state(
        {
          name: 'auth',
          url: '/auth',
          templateUrl: 'auth/auth.html',
          controllerAs: 'vm',
          controller: 'AuthController',
          data: {
            publicAccess: true,
            authorizedAccess: false,
            fullScreen: true,
          },
        });
  }
})();
