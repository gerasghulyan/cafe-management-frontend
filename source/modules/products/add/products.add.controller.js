(function () {
  'use strict';

  angular
    .module('app.products')
    .controller('ProductsAddController', controller);

  controller.$inject = [
    '$scope',
    '$state',
    'ProductsFactory'];

  function controller($scope,
                      $state,
                      ProductsFactory) {
    var vm = this;
    vm.addProduct = addProduct;

    function addProduct() {
      ProductsFactory.createProduct({
        name: vm.name,
        amount: vm.amount,
      })
        .then(function (data) {
          $state.go('products.all');
        })
        .catch(function (err) {
          $scope.$emit('showAlert', {
            title: 'Error',
            template: (err.data && err.data.message) ?
              err.data.message :
              angular.toJson(err.data),
          });
        });
    }
  }
})();
