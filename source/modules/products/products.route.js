(function () {
  'use strict';

  angular.module('app.products').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('products', {
        url: '/products',
        abstract: true,
        templateUrl: 'products/products.layout.html',
        redirectTo: 'products.all',
      })
      .state('products.add', {
        url: '/add',
        controller: 'ProductsAddController',
        controllerAs: 'vm',
        templateUrl: 'products/add/products.add.html',
        data: {
          title: 'Add Product',
          publicAccess: false,
        },
      })
      .state('products.all', {
        url: '/all',
        controller: 'ProductsAllController',
        controllerAs: 'vm',
        templateUrl: 'products/all/products.all.html',
        data: {
          title: 'All Products',
          publicAccess: false,
        },
      })
    ;
  }
})();
