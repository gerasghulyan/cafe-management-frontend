(function () {
  'use strict';

  angular
    .module('app.products')
    .controller('ProductsAllController', controller);

  controller.$inject = [
    '$log',
    'ProductsFactory'];

  function controller($log,
                      ProductsFactory) {
    var vm = this;

    activate();

    function activate() {
      getProducts();
    }

    function getProducts() {
      ProductsFactory.getProducts()
        .then(function (data) {
          vm.products = data.data;
        })
        .catch($log.log.bind($log));
    }
  }
})();
