(function () {
  'use strict';

  angular
    .module('app.products')
    .factory('ProductsFactory', factory);

  factory.$inject = ['$http', 'API_CONFIG'];

  function factory($http, API_CONFIG) {
    var factory = {
      getProducts: getProducts,
      createProduct: createProduct
    };

    return factory;

    function getProducts() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/products',
      });
    }

    function createProduct(product) {
      return $http({
        method: 'POST',
        url: API_CONFIG.getUrl() + '/products',
        data: product,
      });
    }
  }
})();
