(function () {
  'use strict';

  angular.module('app.users').config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('users', {
        url: '/users',
        abstract: true,
        templateUrl: 'users/users.layout.html',
        redirectTo: 'users.all',
      })
      .state('users.add', {
        url: '/add',
        controller: 'UsersAddController',
        controllerAs: 'vm',
        templateUrl: 'users/add/users.add.html',
        data: {
          title: 'Add User',
          publicAccess: false,
        },
      })
      .state('users.all', {
        url: '/all',
        controller: 'UsersAllController',
        controllerAs: 'vm',
        templateUrl: 'users/all/users.all.html',
        data: {
          title: 'All Users',
          publicAccess: false,
        },
      })
    ;
  }
})();
