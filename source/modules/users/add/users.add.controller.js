(function () {
  'use strict';

  angular
    .module('app.users')
    .controller('UsersAddController', controller);

  controller.$inject = [
    '$scope',
    '$state',
    'UsersFactory'];

  function controller($scope,
                      $state,
                      UsersFactory) {
    var vm = this;
    vm.addUser = addUser;

    vm.roles = [
      {
        value: 'MANAGER',
        name: 'Manager'
      },
      {
        value: 'WAITER',
        name: 'Waiter'
      }
    ];

    function addUser() {
      UsersFactory.createUser({
        fullName: vm.fullName,
        username: vm.username,
        password: vm.password,
        role: vm.role
      })
        .then(function (data) {
          $state.go('users.all');
        })
        .catch(function (err) {
          $scope.$emit('showAlert', {
            title: 'Error',
            template: (err.data && err.data.message) ?
              err.data.message :
              angular.toJson(err.data),
          });
        });
    }
  }
})();
