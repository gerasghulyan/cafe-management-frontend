(function () {
  'use strict';

  angular
    .module('app.users')
    .factory('UsersFactory', factory);

  factory.$inject = ['$http', 'API_CONFIG'];

  function factory($http, API_CONFIG) {
    var factory = {
      getUsers: getUsers,
      createUser: createUser,
      getWaiterUsers: getWaiterUsers,
    };

    return factory;

    function getUsers() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/users',
      });
    }

    function createUser(user) {
      return $http({
        method: 'POST',
        url: API_CONFIG.getUrl() + '/users',
        data: user,
      });
    }

    function getWaiterUsers() {
      return $http({
        method: 'GET',
        url: API_CONFIG.getUrl() + '/users/waiters',
      });
    }
  }
})();
