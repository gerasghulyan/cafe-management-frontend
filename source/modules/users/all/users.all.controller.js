(function () {
  'use strict';

  angular
    .module('app.users')
    .controller('UsersAllController', controller);

  controller.$inject = [
    '$log',
    'UsersFactory'];

  function controller($log,
                      UsersFactory) {
    var vm = this;

    activate();

    function activate() {
      getUsers();
    }

    function getUsers() {
      UsersFactory.getUsers()
        .then(function (data) {
          vm.users = data.data;
        })
        .catch($log.log.bind($log));
    }
  }
})();
