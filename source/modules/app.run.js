(function () {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  runBlock.$inject = [
    '$http',
    '$rootScope',
    '$location',
    '$log',
    'AuthFactory',
    '$state',
    '$window',
    'authManager',
    'APP',
  ];

  function runBlock($http,
                    $rootScope,
                    $location,
                    $log,
                    AuthFactory,
                    $state,
                    $window,
                    authManager,
                    APP) {

    $log.debug('Application Started!');

    authManager.checkAuthOnRefresh();
    authManager.redirectWhenUnauthenticated();

    $http.defaults.useXDomain = true;
    $http.defaults.withCredentials = false;

    var stateChangeStart = $rootScope.$on('$stateChangeStart',
      function (event, toState, toParams, fromState, fromParams) {
        $log.debug('State Change Started!');
        $log.debug(event, toState, toParams, fromState, fromParams);

        if (toParams.redirectTo) {
          $log.debug('Redirecting to ', toParams.redirectTo);
          event.preventDefault();
          $state.go(toParams.redirectTo, toParams);
        }

        if (toState.hasOwnProperty('data')
          && toState.data.publicAccess
          && !toState.data.authAccess) {
          $log.debug('State has Public Access');
          if (toState.name === APP.authState && AuthFactory.isAuthenticated()) {
            $state.go(APP.mainState);
            $location.path('/');
          }
          return;
        }

        if (toState.hasOwnProperty('data')
          && toState.data.publicAccess
          && toState.data.authAccess) {
          $log.debug('State has both Public and Auth Access');
          if (AuthFactory.isAuthenticated()) {
            $log.debug('User is Authenticated');
            $state.go(APP.mainState);
            $location.path('/');
          } else {
            $log.debug('User is not Authenticated');
            return;
          }
        }

        if (!AuthFactory.isAuthenticated()) {
          $log.debug('User is Not Authenticated');
          event.preventDefault();
          $state.go(APP.authState);
          $location.path('/');
        }
      });

    var stateChangeSuccess = $rootScope.$on('$stateChangeSuccess',
      function (event, toState, toParams, fromState, fromParams) {
        $log.debug('State Change Successful!');
        $log.debug(event, toState, toParams, fromState, fromParams);
        $rootScope.fullScreen = false;
        if (toState.hasOwnProperty('data')) {
          $rootScope.fullScreen = toState.data.fullScreen;
        }
        if (toState.hasOwnProperty('data') && toState.data.title) {
          $rootScope.currentPage = toState.data.title;
        } else {
          String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
          };
          $rootScope.currentPage = toState.name.capitalize();
        }
      });

    var stateChangeError = $rootScope.$on('$stateChangeError',
      function (event, toState, toParams, fromState, fromParams, error) {
        $log.debug('State Change Error!');
        $log.debug(event, toState, toParams, fromState, fromParams, error);
        if ($window.handlingRouteChangeError) {
          return;
        }
        $window.handlingRouteChangeError = true;
        var destination = (fromState && (fromState.title ||
          fromState.name || fromState.loadedTemplateUrl)) ||
          'unknown target';
        var msg = 'Error routing to ' + destination + '. ' + (error.msg || '');
        $log.warn(msg, [fromState]);
        $location.path('/');
      });

    var rootScopeDestroyListener = $rootScope.$on('$destroy', function () {
      stateChangeStart();
      stateChangeSuccess();
      stateChangeError();
      rootScopeDestroyListener();
    });
  }
})();
