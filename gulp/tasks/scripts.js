(function () {
  'use strict';
  let gulp = require('gulp');
  let $ = require('gulp-load-plugins')();
  let handleErrors = require('../util/handleErrors');
  let browserSync = require('browser-sync');

  gulp.task('scripts:libraries', function () {
    return gulp.src([
      './node_modules/viewport-units-buggyfill/viewport-units-buggyfill.js',
      './node_modules/hammerjs/hammer.js',

      './node_modules/angular/angular.js',
      './node_modules/angular-animate/angular-animate.js',
      './node_modules/angular-aria/angular-aria.js',
      './node_modules/angular-message-format/angular-message-format.js',
      './node_modules/angular-messages/angular-messages.js',
      './node_modules/angular-sanitize/angular-sanitize.js',
      './node_modules/angular-touch/angular-touch.js',

      './node_modules/angular-ui-router/release/angular-ui-router.js',

      './node_modules/angular-jwt/dist/angular-jwt.js',
      './node_modules/angular-loading-bar/src/loading-bar.js',

      './node_modules/angular-hammer/angular.hammer.js',
      './node_modules/angular-gettext/dist/angular-gettext.js',

      './node_modules/angular-material/angular-material.js',

      './node_modules/ng-idle/angular-idle.js',

      './node_modules/angular-material-data-table/dist/md-data-table.js',

    ])
      .pipe($.ngAnnotate({add: true}))
      .on('error', handleErrors)
      .pipe($.concat('libraries.js', {newLine: ';'}))
      .on('error', handleErrors)
      .pipe($.uglify({mangle: true}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/temp'));
  });

  gulp.task('scripts:application', function () {
    return gulp.src([
      './source/modules/*.module.js',
      './source/modules/*.js',
      './source/modules/**/*.module.js',
      './source/modules/**/*.js',
    ])
      .pipe($.ngAnnotate())
      .on('error', handleErrors)
      .pipe($.concat('application.js', {newLine: ';'}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/temp'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('scripts', ['scripts:libraries', 'scripts:application']);
})();
