(function () {
  'use strict';
  let gulp = require('gulp');
  let $ = require('gulp-load-plugins')();
  let browserSync = require('browser-sync');
  let handleErrors = require('../util/handleErrors');

  gulp.task('l10n:html2pot', function () {
    return gulp.src([
      './source/index.html',
      './source/modules/**/*.html',
      './source/modules/**/*.js'])
      .pipe($.angularGettext.extract('l10n.pot', {}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./source/l10n/'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('l10n:po2json', function () {
    return gulp.src('./source/l10n/**/*.po')
      .pipe($.angularGettext.compile({format: 'json'}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/l10n/'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('l10n', ['l10n:html2pot', 'l10n:po2json']);
})();
