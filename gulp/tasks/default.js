(function () {
  'use strict';
  let gulp = require('gulp');
  let rimraf = require('rimraf');
  let $ = require('gulp-load-plugins')();
  let handleErrors = require('../util/handleErrors');
  let browserSync = require('browser-sync');

  gulp.task('clean', function (cb) {
    return rimraf('./public', cb);
  });

  gulp.task('clear', ['clean']);

  gulp.task('default', ['build', 'index:dev', 'watch']);

  gulp.task('build', ['fonts', 'images', 'l10n', 'scripts', 'styles', 'views']);

  gulp.task('browserSync', function () {
    return browserSync({
      server: {baseDir: './public'},
      https: false,
      ghostMode: {
        clicks: true,
        forms: true,
        scroll: true,
      },
      logLevel: 'debug',
      logConnections: true,
    });
  });

  gulp.task('watch', ['browserSync'], function () {
    gulp.watch(['./source/fonts/**'], ['fonts']);
    gulp.watch(['./source/images/**/*'], ['images']);
    gulp.watch(['./source/l10n/**/*.*'], ['l10n:po2json']);
    gulp.watch(['./source/modules/**/*.js'], [
      'scripts:application',
      'l10n:html2pot',
    ]);
    gulp.watch(['./source/styles/**/*'], ['styles:application']);
    gulp.watch(['./source/modules/**/*.html'], [
      'views',
      'l10n:html2pot']);
    gulp.watch(['./source/index.html'], ['index:dev']);
  });

  gulp.task('index:dev', function () {
    return gulp.src('./source/index.html')
      .pipe($.htmlReplace({
        'js': [
          'temp/libraries.js',
          'temp/application.js',
          'temp/templates.js',
        ],
        'css': [
          'temp/libraries.css',
          'temp/application.css',
        ],
      }))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('index:prod', function () {
    return gulp.src('./source/index.html')
      .pipe($.htmlReplace({
        'js': ['scripts/app.js'],
        'css': ['styles/app.css'],
      }))
      .on('error', handleErrors)
      .pipe($.htmlmin({removeComments: true, collapseWhitespace: true}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public'));
  });

  gulp.task('prod', ['build', 'index:prod'], function () {
    gulp.src([
      './public/temp/libraries.js',
      './public/temp/application.js',
      './public/temp/templates.js',
    ])
      .pipe($.concat('app.js', {newLine: ';'}))
      .on('error', handleErrors)
      .pipe($.uglify({mangle: true}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/scripts'));

    gulp.src([
      './public/temp/libraries.css',
      './public/temp/application.css',
    ])
      .pipe($.concat('app.css'))
      .on('error', handleErrors)
      .pipe($.autoprefixer())
      .on('error', handleErrors)
      .pipe($.cssnano())
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/styles'));
  });
})();
