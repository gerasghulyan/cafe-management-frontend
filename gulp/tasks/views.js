(function () {
  'use strict';
  let fs = require('fs');
  let gulp = require('gulp');
  let $ = require('gulp-load-plugins')();
  let browserSync = require('browser-sync');
  let handleErrors = require('../util/handleErrors');

  gulp.task('views', function () {
    if (!fs.existsSync('./public')) fs.mkdirSync('./public');
    if (!fs.existsSync('./public/temp')) fs.mkdirSync('./public/temp');
    return gulp.src('./source/modules/**/*.html')
      .pipe($.htmlmin())
      .on('error', handleErrors)
      .pipe($.angularTemplatecache('templates.js', {
        standalone: true,
      }))
      .on('error', handleErrors)
      .pipe($.uglify({mangle: true}))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/temp'))
      .pipe(browserSync.reload({stream: true}));
  });
})();
