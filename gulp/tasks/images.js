(function () {
  'use strict';
  let gulp = require('gulp');
  let $ = require('gulp-load-plugins')();
  let browserSync = require('browser-sync');
  let handleErrors = require('../util/handleErrors');
  gulp.task('images', function () {
    gulp.src(['./source/favicon.ico'])
      .pipe(gulp.dest('./public'));

    return gulp.src('./source/images/**/*.*')
      .pipe($.image({
        pngquant: true,
        optipng: true,
        zopflipng: true,
        advpng: true,
        jpegRecompress: true,
        jpegoptim: true,
        mozjpeg: true,
        gifsicle: true,
        svgo: {
          enable: ['removeRasterImages'],
          disable: ['removeDoctype', 'removeViewBox'],
        },
      }))
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/images'))
      .pipe(browserSync.reload({stream: true}));
  });
})();
