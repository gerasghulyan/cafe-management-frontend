(function () {
  'use strict';
  let gulp = require('gulp');
  let browserSync = require('browser-sync');

  gulp.task('fonts', function () {
    gulp.src(['./node_modules/font-awesome/fonts/**/*'], {
      dot: true,
      base: './node_modules/font-awesome/',
    })
      .pipe(gulp.dest('./public'))
      .pipe(browserSync.reload({stream: true}));

    gulp.src(['./source/fonts/**/*'], {
      dot: true,
      base: './source/',
    })
      .pipe(gulp.dest('./public'))
      .pipe(browserSync.reload({stream: true}));
  });
})();
