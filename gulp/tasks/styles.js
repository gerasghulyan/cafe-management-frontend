(function () {
  'use strict';
  let gulp = require('gulp');
  let $ = require('gulp-load-plugins')();
  let handleErrors = require('../util/handleErrors');
  let browserSync = require('browser-sync');

  gulp.task('styles:libraries', function () {
    return gulp.src('./source/styles/libraries.scss')
      .pipe($.sass({
        includePaths: ['source/styles'],
        errLogToConsole: true,
        sourceComments: 'map',
        outputStyle: 'expanded',
        imagePath: '/images',
      }))
      .on('error', handleErrors)
      .pipe($.autoprefixer())
      .on('error', handleErrors)
      .pipe($.cssnano())
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/temp'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('styles:application', function () {
    return gulp.src('./source/styles/application.scss')
      .pipe($.sass({
        includePaths: ['source/styles'],
        errLogToConsole: true,
        sourceComments: 'map',
        outputStyle: 'expanded',
        imagePath: '/images',
      }))
      .on('error', handleErrors)
      .pipe($.autoprefixer())
      .on('error', handleErrors)
      .pipe($.cssnano())
      .on('error', handleErrors)
      .pipe(gulp.dest('./public/temp'))
      .pipe(browserSync.reload({stream: true}));
  });

  gulp.task('styles', ['styles:libraries', 'styles:application']);
})();
